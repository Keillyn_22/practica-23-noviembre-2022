import React, { useState } from 'react'
import styled from 'styled-components'
import Level1 from './level1'

const Container = styled.div`

display: flex;
justify-content: center;
align-items: center;


   .cuadrado {
     width: 200px;
     height: 350px;
     border: solid 1px black;
     background: white;
     cursor: pointer;
     positon:absolute

   }
 
`

const App = (): JSX.Element => {

   const [position, setPosition] = useState(false);

   const [color, setColor] = useState(false);


     const handleClickColor = () => {
        
      if(color != true) setColor(true);

            else setColor(false);
              
       }


     const handleClick = () => {

          if(position != true) setPosition(true);

            else setPosition(false)
       }


    document.addEventListener("mouseup", function(event:any){   

      var click = document.getElementById("cuadrado");

      if(!click?.contains(event.target)){

            handleClickColor();
      } else {

            handleClick();
      }


    })
     

  return (

     <Container >

        <div className='cuadrado'  id="cuadrado"  >

        </div>

        <Level1 position={position} color={color} />
          
     </Container>

    )
}

export default App;
