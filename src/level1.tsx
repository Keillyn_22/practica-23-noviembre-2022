import styled from 'styled-components'

const Container = styled.div<{ position: boolean, varcolor:boolean}>`
 width: 125px;
 height: 125px;
 bottom: 0px;
 position: absolute;
 top:14.7rem;
 right: ${props => props.position ? '30rem' : '22.1rem'};
 background: ${props => props.varcolor ? 'red' : 'green'};
`

export interface params {
    position: boolean,
    color: boolean

}

const App = (params:params): JSX.Element => {
     
    return (
  
      <Container position={params.position} varcolor={params.color} >
          
      </Container>

    )
}


export default App;
