import styled from 'styled-components'
import Level2 from "./level2"

const Container = styled.div`

    background: #0cd70c17;
    height: 100vh;
    margin: -20px;
    padding: 20px; 

`

const App = (): JSX.Element => {


    return (

        <Container >
            
         <Level2 />
                  
        </Container>

    )
}


export default App;
